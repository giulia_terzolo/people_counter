
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:flutter/material.dart';



class History extends StatefulWidget {
  final email;
  final citta;
  final numFermata;
  History({Key?key,this.email, this.citta, this.numFermata}) : super(key: key);
  @override
  _HistoryState createState() => new _HistoryState();
}



class _HistoryState extends State<History> {

  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);



  Container _buildHeader() {
    String docName=widget.citta+"_"+widget.numFermata;
    String title= "GRAFICO";
    // ignore: sized_box_for_whitespace
    return Container(
      height: 250,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Column(
            children: [
              const SizedBox(height: 60.0,),
              Row(
                children: [

                  const Spacer(),

                  FloatingActionButton(
                    backgroundColor: color3,
                    child: const Icon(Icons.arrow_back, size: 45.0, color: Colors.white,),
                    //Icon(FontAwesomeIcons.sistrix, size: 35.0,),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                  const SizedBox(width: 20.0,),



                ],
              ),
            ],
          ),

          Positioned(
            bottom: 0,
            left: -100,
            top: -150,
            child: Container(
              width: 350,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color1, color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color2,
                        offset: const Offset(4.0,4.0),
                        blurRadius: 10.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    colors: [color3,color2]
                ),
                boxShadow: [
                  BoxShadow(
                      color: color3,
                      offset: const Offset(1.0,1.0),
                      blurRadius: 4.0
                  )
                ]
            ),
          ),
          Positioned(
            top: 100,
            right: 200,
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color3,color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color3,
                        offset: const Offset(1.0,1.0),
                        blurRadius: 4.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 60,
                left: 30
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:   <Widget>[
                const Text("People Counter", style:  TextStyle(
                    color: Colors.white,
                    fontSize: 28.0,
                    fontWeight: FontWeight.w700
                ),),
                const SizedBox(height: 10.0),
                Text(title, style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),


              ],
            ),
          )

        ],

      ),

    );
  }



  @override
  Widget build(BuildContext context) {
    String cod=widget.citta.toString().toLowerCase()+"_"+widget.numFermata.toString().toLowerCase();
    String titolo ="GRAFICO "+widget.citta+" - FERMATA "+widget.numFermata;
    const Color color3 = Color(0xffFB8964);
    return Scaffold(




      body: SingleChildScrollView(
          child: Column(
              children:<Widget>[
                _buildHeader(),
                const SizedBox(height: 10,),
                StreamBuilder<DocumentSnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('grafici')
                        .doc(cod)
                        .snapshots(),
                    builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasData) {
                        print(snapshot);
                        int max = snapshot.data!["max"];
                        //10 persone
                        int p1 = snapshot.data!["p1"];
                        DateTime t1 = snapshot.data!["t1"].toDate();
                        int p2 = snapshot.data!["p2"];
                        DateTime t2 = snapshot.data!["t2"].toDate();
                        int p3 = snapshot.data!["p3"];
                        DateTime t3 = snapshot.data!["t3"].toDate();
                        int p4 = snapshot.data!["p4"];
                        DateTime t4 = snapshot.data!["t4"].toDate();
                        int p5 = snapshot.data!["p5"];
                        DateTime t5 = snapshot.data!["t5"].toDate();
                        int p6 = snapshot.data!["p6"];
                        DateTime t6 = snapshot.data! ["t6"].toDate();
                        int p7 = snapshot.data!["p7"];
                        DateTime t7 = snapshot.data!["t7"].toDate();
                        int p8 = snapshot.data!["p8"];
                        DateTime t8 = snapshot.data!["t8"].toDate();
                        int p9 = snapshot.data!["p9"];
                        DateTime t9 = snapshot.data!["t9"].toDate();
                        int p10 = snapshot.data!["p10"];
                        DateTime t10 = snapshot.data!["t10"].toDate();

                        //20 persone
                        int p11 = snapshot.data!["p11"];
                        DateTime t11 = snapshot.data!["t11"].toDate();
                        int p12 = snapshot.data!["p12"];
                        DateTime t12 = snapshot.data!["t12"].toDate();
                        int p13 = snapshot.data!["p13"];
                        DateTime t13 = snapshot.data!["t13"].toDate();
                        int p14 = snapshot.data!["p14"];
                        DateTime t14 = snapshot.data!["t14"].toDate();
                        int p15 = snapshot.data!["p15"];
                        DateTime t15 = snapshot.data!["t15"].toDate();
                        int p16 = snapshot.data!["p16"];
                        DateTime t16 = snapshot.data! ["t16"].toDate();
                        int p17 = snapshot.data!["p17"];
                        DateTime t17 = snapshot.data!["t17"].toDate();
                        int p18 = snapshot.data!["p18"];
                        DateTime t18 = snapshot.data!["t18"].toDate();
                        int p19 = snapshot.data!["p19"];
                        DateTime t19 = snapshot.data!["t19"].toDate();
                        int p20 = snapshot.data!["p20"];
                        DateTime t20 = snapshot.data!["t20"].toDate();


                        //10 bus
                        DateTime bt1 = snapshot.data!["bt1"].toDate();
                        DateTime bt2 = snapshot.data!["bt2"].toDate();
                        DateTime bt3 = snapshot.data!["bt3"].toDate();
                        DateTime bt4 = snapshot.data!["bt4"].toDate();
                        DateTime bt5 = snapshot.data!["bt5"].toDate();

                        //10 bus
                        String b1 = snapshot.data!["b1"];
                        String b2 = snapshot.data!["b2"];
                        String b3 = snapshot.data!["b3"];
                        String b4 = snapshot.data!["b4"];
                        String b5 = snapshot.data!["b5"];


                        //desc bus 10
                        String bus1 = "bus "+b1.toLowerCase();
                        String bus2 = "bus "+b2.toLowerCase();
                        String bus3 = "bus "+b3.toLowerCase();
                        String bus4 = "bus "+b4.toLowerCase();
                        String bus5 = "bus "+b5.toLowerCase();




                        return Container(
                            child: SfCartesianChart(
                              primaryXAxis: CategoryAxis(),
                              title: ChartTitle(text: titolo.toUpperCase(), textStyle: const TextStyle(fontWeight: FontWeight.bold), borderColor: color3), //Chart title.
                              legend: Legend(isVisible: true), // Enables the legend.
                              tooltipBehavior: TooltipBehavior(enable: true), // Enables the tooltip.
                              series: <LineSeries<IntData, DateTime>>[
                                LineSeries<IntData, DateTime>(
                                    legendItemText:"num. persone",
                                    dataSource: [
                                      //10
                                      IntData(t1, p1),
                                      IntData(t2, p2),
                                      IntData(t3, p3),
                                      IntData(t4, p4),
                                      IntData(t5, p5),
                                      IntData(t6, p6),
                                      IntData(t7, p7),
                                      IntData(t8, p8),
                                      IntData(t9, p9),
                                      IntData(t10, p10),

                                      //20
                                      IntData(t11, p11),
                                      IntData(t12, p12),
                                      IntData(t13, p13),
                                      IntData(t14, p14),
                                      IntData(t15, p15),
                                      IntData(t16, p16),
                                      IntData(t17, p17),
                                      IntData(t18, p18),
                                      IntData(t19, p19),
                                      IntData(t20, p20),


                                    ],
                                    xValueMapper: (IntData data, _) => data.time,
                                    yValueMapper: (IntData data, _) => data.p,
                                    dataLabelSettings: const DataLabelSettings(isVisible: true) // Enables the data label.
                                ),


                                //bus 1
                                LineSeries<IntData, DateTime>(

                                    legendItemText:bus1,
                                    dataSource: [
                                      IntData(bt1, 0),
                                      IntData(bt1, max),
                                    ],
                                    xValueMapper: (IntData data1, _) => data1.time,
                                    yValueMapper: (IntData data1, _) => data1.p,
                                    dataLabelSettings: const DataLabelSettings(isVisible: true) // Enables the data label.
                                ),

                                //bus 2
                                LineSeries<IntData, DateTime>(
                                    legendItemText:bus2,
                                    dataSource: [
                                      IntData(bt2, 0),
                                      IntData(bt2, max),
                                    ],
                                    xValueMapper: (IntData data2, _) => data2.time,
                                    yValueMapper: (IntData data2, _) => data2.p,
                                    dataLabelSettings: const DataLabelSettings(isVisible: true) // Enables the data label.
                                ),

                                //bus 3
                                LineSeries<IntData, DateTime>(
                                    legendItemText:bus3,
                                    dataSource: [
                                      IntData(bt3, 0),
                                      IntData(bt3, max),
                                    ],
                                    xValueMapper: (IntData data3, _) => data3.time,
                                    yValueMapper: (IntData data3, _) => data3.p,
                                    dataLabelSettings: const DataLabelSettings(isVisible: true) // Enables the data label.
                                ),

                                //bus 4
                                LineSeries<IntData, DateTime>(
                                    legendItemText:bus4,
                                    dataSource: [
                                      IntData(bt4, 0),
                                      IntData(bt4, max),
                                    ],
                                    xValueMapper: (IntData data4, _) => data4.time,
                                    yValueMapper: (IntData data4, _) => data4.p,
                                    dataLabelSettings: const DataLabelSettings(isVisible: true) // Enables the data label.
                                ),

                                //bus 5
                                LineSeries<IntData, DateTime>(
                                    legendItemText:bus5,
                                    dataSource: [
                                      IntData(bt5, 0),
                                      IntData(bt5, max),
                                    ],
                                    xValueMapper: (IntData data5, _) => data5.time,
                                    yValueMapper: (IntData data5, _) => data5.p,
                                    dataLabelSettings: const DataLabelSettings(isVisible: true) // Enables the data label.
                                ),



                              ],

                            )
                        );
                      }
                      return const Text("ERRORE");
                    }
                ),


              ]
          )
      ),);
  }}


class IntData {
  IntData(this.time, this.p);
  final DateTime time;
  final int p;
}

