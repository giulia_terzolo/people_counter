import 'package:flutter/material.dart';

var lightThemeData = ThemeData(

    //light theme

    primaryColor:  Colors.orange[200],
    primaryColorBrightness: Brightness.light,
    primaryColorLight: Colors.orange[50],
    brightness: Brightness.light,
    primaryColorDark: Colors.orange[50],
    //indicatorColor: Colors.white ,
    canvasColor: Colors.orange[50],
);

var darkThemeData = ThemeData(
  //dark theme

    primaryColor: Colors.grey[900],
    primaryColorBrightness: Brightness.dark,
    primaryColorLight: Colors.grey[850],
    brightness: Brightness.dark,
    primaryColorDark: Colors.grey[850],
   //indicatorColor: Colors.white ,
    canvasColor: Colors.grey[850],

);