import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_dynamic_theme/easy_dynamic_theme.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'screens/home.dart';
import 'login/login.dart';
import 'theme/theme.dart';

void main() async {
  runApp(
    EasyDynamicThemeWidget(
      child: const MyApp(),
    ),

  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));

    return MaterialApp(

      builder: (context, child) {
        return MediaQuery(
          child: child!,

          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },

      title: 'autoBUS',
      theme: lightThemeData,
      darkTheme: darkThemeData,
      themeMode: EasyDynamicTheme.of(context).themeMode,
      debugShowCheckedModeBanner: false,
      home: LandingPage(),
    );
  }
}


//FirebaseFirestore _db = FirebaseFirestore.instance;
class LandingPage extends StatelessWidget{
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  LandingPage({Key? key}) : super(key: key);



  void createHomePage({
    required BuildContext context
  })  async {
    DocumentSnapshot document;
    final _auth = FirebaseAuth.instance;
    dynamic user;
    try {
      FirebaseFirestore _db = FirebaseFirestore.instance;
      user = _auth.currentUser;
      String _userEmail = user.email;
      print(_userEmail);
      document = await   _db.collection('utenti').doc(_userEmail).get();
      print(document.data());

      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Home(email: _userEmail,),));

    } catch (e) {
      print(e);
    }
  }



  @override
  Widget build(BuildContext context){
    return FutureBuilder(
        future : _initialization,
        builder: (context, snapshot) {
          if(snapshot.hasError){
            return Scaffold(
              body: Center(
                child: Text("Error: ${snapshot.error}"),
              ),
            );
          }
          if(snapshot.connectionState == ConnectionState.done){
            return StreamBuilder(
                stream: FirebaseAuth.instance.authStateChanges(),
                builder: (context, snapshot) {
                  if(snapshot.connectionState == ConnectionState.active){
                    Object? user = snapshot.data;

                    if(user == null){
                      return const LoginPage();
                    } else{
                      createHomePage(context: context);
                    }

                  }
                  return const Scaffold(
                    body: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Colors.deepOrangeAccent,
                        ),
                      ),
                    ),
                  );

                }
            );
          }
          return const Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Colors.deepOrangeAccent,
                ),
              ),
            ),
          );

        }
    );
  }
}


