
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../home.dart';

FirebaseFirestore _db = FirebaseFirestore.instance;

class Addcityitem extends StatefulWidget {
  final email;
  Addcityitem({Key?key,this.email}) : super(key: key);
  @override
  AddcityitemState createState() => new AddcityitemState();
}



class AddcityitemState extends State<Addcityitem>{
  late String _nome;
  late String _sigla;
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  void setInfo({
    required String email,
  })  async {
    try {

      await _db.collection("citta").doc().set({
        "email": email,
        "nome": _nome.toLowerCase(),
        "sigla":_sigla.toLowerCase(),
      });
    } catch (e) {
      print(e);
    }
  }

  Widget _buildNomeCitta(){
    return TextFormField(
      keyboardType: TextInputType.text,
      //style:  TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),
      decoration:  InputDecoration(
        labelText: "Nome città",
        labelStyle:   TextStyle(color: color1, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        hintText: "Inserire nome città ...",
        hintStyle: TextStyle(
          color: color3,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "Il nome della città deve essere inserito";
        }
      }, onSaved: (String? value){
      _nome = value!;
    },
    );
  }


  Widget _buildSiglaCitta(){
    return TextFormField(
      keyboardType: TextInputType.text,
      inputFormatters: [
        LengthLimitingTextInputFormatter(2),
      ],
      //style:  TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),
      decoration:  InputDecoration(
        labelText: "Provincia della città in sigla",
        labelStyle:   TextStyle(color: color1, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        hintText: "Inserire la provincia della città in sigla ...",
        hintStyle: TextStyle(
          color: color3,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "La sigla della provincia della città deve essere inserita";
        }
      }, onSaved: (String? value){
      _sigla = value!;
    },
    );
  }




  @override
  Widget build(BuildContext context){
    print(widget.email);
    return Scaffold(
      appBar: AppBar(
        title: const Text("AGGIUNGI CITTA'", style: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),),
        backgroundColor: color3,
      ),
      body: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[

            Container(

              margin: const EdgeInsets.symmetric(),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(height: 60,),
                    _buildNomeCitta(),
                    const SizedBox(height: 20,),
                    _buildSiglaCitta(),
                    const SizedBox(height: 50,),
                    RaisedButton(
                      color: Colors.red,
                      child: const Text("CREA",
                        style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
                      onPressed:(){
                        if(!_formKey.currentState!.validate()){
                          return;
                        }
                        _formKey.currentState!.save();
                        print(_nome);
                        print(_sigla);
                        setInfo(email: widget.email);
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.green, content: Text("Città inserita correttamente.",
                          style: TextStyle(color: Colors.black, ),)));
                        Navigator.pop(context);
                        //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Home(email: widget.email,),));
                      },

                    ),
                    const SizedBox(height: 20,),


                  ],
                ),
              ),

            ),

          ],
        ),
    );
  }
}