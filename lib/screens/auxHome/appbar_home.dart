import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'add_city.dart';
import 'search_city.dart';

class AppBarTitle extends StatefulWidget {
  final email;
  AppBarTitle({Key?key,this.email}) : super(key: key);
  @override
  _AppBarTitleState createState() =>  _AppBarTitleState();
}



class _AppBarTitleState extends State<AppBarTitle> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);






  Container _buildHeader() {
    // ignore: sized_box_for_whitespace
    return Container(
      height: 250,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Column(
            children: [
              const SizedBox(height: 30.0,),
              Row(
                children: [
                  const Spacer(),
                  FloatingActionButton(
                    backgroundColor: color3,
                    child: const Icon(Icons.search, size: 45.0, color: Colors.white,),
                    //Icon(FontAwesomeIcons.sistrix, size: 35.0,),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => CloudFirestoreSearchCity(email:  widget.email,)),);
                    },
                  ),
                  const SizedBox(width: 20.0,),
                ],
              ),
            ],
          ),
          Column(
            children: [
              const SizedBox(height: 100.0,),
              Row(
                children: [
                  const Spacer(),
                  FloatingActionButton(
                    backgroundColor: color3,
                    child: const Icon(FontAwesomeIcons.plus, size: 45.0, color: Colors.white,),
                    //Icon(FontAwesomeIcons.sistrix, size: 35.0,),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Addcityitem(email:  widget.email,)),);
                    },
                  ),
                  const SizedBox(width: 20.0,),
                ],
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            left: -100,
            top: -150,
            child: Container(
              width: 350,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color1, color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color2,
                        offset: const Offset(4.0,4.0),
                        blurRadius: 10.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    colors: [color3,color2]
                ),
                boxShadow: [
                  BoxShadow(
                      color: color3,
                      offset: const Offset(1.0,1.0),
                      blurRadius: 4.0
                  )
                ]
            ),
          ),
          Positioned(
            top: 100,
            right: 200,
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color3,color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color3,
                        offset: const Offset(1.0,1.0),
                        blurRadius: 4.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 60,
                left: 30
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:  <Widget>[
                StreamBuilder<DocumentSnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('utenti')
                        .doc(widget.email)
                        .snapshots(),
                    builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasData) {
                        print(snapshot);
                        String nic = snapshot.data!.get("nick");
                        String nome= "Buongiorno "+nic.toUpperCase()+"!";
                        return Text(nome, textAlign: TextAlign.center, style: const TextStyle(fontSize: 18.0, color: Colors.white,),);}
                      else {
                        return const Text("ERRORE");
                      }}),
                const SizedBox(height: 10.0),
                const Text("People Counter", style: TextStyle(
                    color: Colors.white,
                    fontSize: 28.0,
                    fontWeight: FontWeight.w700
                ),),
                const SizedBox(height: 10.0),
                const Text("CITTA'", style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),


            ],
            ),
          )

        ],

      ),

    );
  }


  @override
  Widget build(BuildContext context) {
    return Theme(
          data: Theme.of(context).copyWith(),
      child: _buildHeader(),
      /*SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildHeader(),
              //const SizedBox(height: 1.0),
            ],
          ),
        ),*/
    );
  }
}