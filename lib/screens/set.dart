import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_dynamic_theme/easy_dynamic_theme.dart';

import 'home.dart';
import '../login/login.dart';
import 'plots.dart';


class Set extends StatefulWidget {
  final email;
  const Set({Key?key,this.email}) : super(key: key);
  @override
  _SettingsState createState() => new _SettingsState();
}


class _SettingsState extends State<Set> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);



  @override
  Widget build(BuildContext context){
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));

    return Scaffold(

      body: Theme(
        data: Theme.of(context).copyWith(
        ),

        //this creates blocks
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildHeader(),
              const SizedBox(height: 40.0),
              const Divider(height: 20, thickness: 2,),
              ListTile(
                title: const Text("Tema", style: TextStyle(
                  fontSize: 18.0,
                ),),
                trailing: EasyDynamicThemeSwitch(),

              ),
              const Divider(height: 20, thickness: 2,),
              const SizedBox(height: 100.0),
              const Divider(height: 20, thickness: 2,),
              ListTile(
                tileColor: color3,
                title: const Text("LOGOUT", style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0,
                ),),
                trailing: const Icon(Icons.logout),
                onTap: () async {
                  await FirebaseAuth.instance.signOut();
                  Navigator.of(context)
                      .pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (context) => const LoginPage()
                    ),
                        (_) => false,
                  );
                },

              ),
              const Divider(height: 20, thickness: 2,),






    const SizedBox(height: 30.0),
            ],
          ),
        ),
      ),


      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: SizedBox(
          height: 50,
          child: Row(
            children: <Widget>[
              const SizedBox(width: 20.0),
              const SizedBox(width: 45.0),
              const Spacer(),
              //const SizedBox(width: 20.0),
              IconButton(
                icon: const Icon(Icons.location_city_rounded, size: 30.0,  ),
                onPressed: (){
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) =>  Home(email: widget.email,),
                      ));
                },
              ),
              const Spacer(),
              IconButton(
                icon: const Icon(FontAwesomeIcons.chartLine),
                onPressed: (){
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) =>  Plots(email: widget.email,),
                      ));
                },
              ),
              //const Spacer(),
              const SizedBox(width: 20.0),

            ],
          ),
        ),
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: color2,
        child: const Icon(FontAwesomeIcons.cog),
        onPressed: (){},
      ),

    );
  }


  Container _buildHeader() {
    // ignore: sized_box_for_whitespace
    return Container(
      height: 250,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0,
            left: -100,
            top: -150,
            child: Container(
              width: 350,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color1, color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color2,
                        offset: const Offset(4.0,4.0),
                        blurRadius: 10.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    colors: [color3,color2]
                ),
                boxShadow: [
                  BoxShadow(
                      color: color3,
                      offset: const Offset(1.0,1.0),
                      blurRadius: 4.0
                  )
                ]
            ),
          ),
          Positioned(
            top: 100,
            right: 200,
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color3,color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color3,
                        offset: const Offset(1.0,1.0),
                        blurRadius: 4.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 60,
                left: 30
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const <Widget>[
                Text("People Counter", style: TextStyle(
                    color: Colors.white,
                    fontSize: 28.0,
                    fontWeight: FontWeight.w700
                ),),
                SizedBox(height: 10.0),
                Text("IMPOSTAZIONI", style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),

              ],
            ),
          )
        ],
      ),
    );
  }
}