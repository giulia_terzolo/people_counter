

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'auxHome/appbar_home.dart';
import 'auxHome/list_home.dart';
import 'set.dart';
import 'plots.dart';

class Home extends StatefulWidget {
  final email;
  Home({Key?key,this.email}) : super(key: key);
  @override
  _HomeState createState() =>  _HomeState();
}



class _HomeState extends State<Home> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);




  @override
  Widget build(BuildContext context){
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));


    return Scaffold(

      appBar: AppBar(
        backgroundColor: Colors.transparent,
        titleSpacing: 0.0,
        elevation: 0,
        title: AppBarTitle(email: widget.email,),
        toolbarHeight: 225.0,
      ),



      body: Theme(
        data: Theme.of(context).copyWith(
    ),

    //this creates blocks
    child: SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          bottom: 20.0,
          top: 0,
        ),
        child: ListC(email: widget.email,),
      ),

    ),

        ),


      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: SizedBox(
          height: 50,
          child: Row(
            children: <Widget>[
              const SizedBox(width: 20.0),
              IconButton(
                //color: Colors.grey.shade700,
                icon: const Icon(FontAwesomeIcons.cog, size: 30,), onPressed:  (){
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) =>  Set(email: widget.email,),
                    ));
              },
              ),
              const Spacer(),
              IconButton(
                //color: Colors.grey.shade700,
                icon: const Icon(FontAwesomeIcons.chartLine,size: 30,), onPressed: (){
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) =>  Plots(email: widget.email,),
                    ));
              },
              ),
              const SizedBox(width: 20.0),
            ],
          ),
        ),
      ),


      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: color2,
        child: const Icon(Icons.location_city_rounded, size: 40.0,  ),
        onPressed: (){},
      ),





    );

  }


}