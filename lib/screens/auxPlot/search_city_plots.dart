import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../busStopPlot/home_stops_plot.dart';


class CloudFirestoreSearchCityPlots extends StatefulWidget {
  final email;
  CloudFirestoreSearchCityPlots({Key?key,this.email}) : super(key: key);
  @override
  _CloudFirestoreSearchCityPlotsState createState() => _CloudFirestoreSearchCityPlotsState();
}


class _CloudFirestoreSearchCityPlotsState extends State<CloudFirestoreSearchCityPlots> {
  String name = "";



  @override
  Widget build(BuildContext context) {

    final Color color3 = const Color(0xffFB8964);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: color3,
        title: Card(
          child: TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(2),
            ],
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: color3),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: color3),
                ),

                prefixIcon: Icon(Icons.search, color: Colors.white,), hintText: 'Ricerca la sigla di una provincia...'),
            onChanged: (val) {
              setState(() {
                name = val;
              });
            },
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 16.0,
            right: 16.0,
            bottom: 20.0,
            top: 20.0,
          ),
          child: StreamBuilder<QuerySnapshot>(
            stream: (name != "" && name != null)
                ? FirebaseFirestore.instance
                .collection('citta')
                .where("sigla", isEqualTo: name.toLowerCase())
                .snapshots()
                : FirebaseFirestore.instance.collection('citta').snapshots(),
            builder: (context, snapshot) {
              return (snapshot.connectionState == ConnectionState.waiting)
                  ? const Center(child: CircularProgressIndicator())
                  :  ListView.separated(
                  separatorBuilder: (context, index) => SizedBox(height: 16.0),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    //var noteInfo = snapshot.data!.docs[index].data()!;
                    var noteInfo = snapshot.data!.docs[index];
                    String docID = snapshot.data!.docs[index].id;
                    String title = noteInfo['nome'];
                    //String cit = noteInfo['citta'];
                    String description = noteInfo['sigla'];
                    return Ink(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: ListTile(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => HomeStopsPlot(email:  widget.email,citta: title,)),);
                        },
                        title: Text(
                          title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle( fontWeight: FontWeight.bold),
                        ),
                        /*subtitle: Text(
                          description,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),*/
                      ),

                    );
                  }

              );
            },
          ),
        ),
      ),
    );
  }

}