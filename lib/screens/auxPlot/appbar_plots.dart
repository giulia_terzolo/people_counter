
import 'package:flutter/material.dart';
import 'package:people_counter/screens/auxPlot/search_city_plots.dart';


class AppBarPlots extends StatefulWidget {
  final email;
  AppBarPlots({Key?key,this.email}) : super(key: key);
  @override
  _AppBarPlotsState createState() =>  _AppBarPlotsState();
}



class _AppBarPlotsState extends State<AppBarPlots> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);






  Container _buildHeader() {
    // ignore: sized_box_for_whitespace
    return Container(
      height: 250,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Column(
            children: [
              const SizedBox(height: 30.0,),
              Row(
                children: [
                  const Spacer(),
                  FloatingActionButton(
                    backgroundColor: color3,
                    child: const Icon(Icons.search, size: 45.0, color: Colors.white,),
                    //Icon(FontAwesomeIcons.sistrix, size: 35.0,),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => CloudFirestoreSearchCityPlots(email:  widget.email,)),);
                    },
                  ),
                  const SizedBox(width: 20.0,),
                ],
              ),
            ],
          ),

          Positioned(
            bottom: 0,
            left: -100,
            top: -150,
            child: Container(
              width: 350,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color1, color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color2,
                        offset: const Offset(4.0,4.0),
                        blurRadius: 10.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    colors: [color3,color2]
                ),
                boxShadow: [
                  BoxShadow(
                      color: color3,
                      offset: const Offset(1.0,1.0),
                      blurRadius: 4.0
                  )
                ]
            ),
          ),
          Positioned(
            top: 100,
            right: 200,
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color3,color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color3,
                        offset: const Offset(1.0,1.0),
                        blurRadius: 4.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 60,
                left: 30
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const <Widget>[
                Text("People Counter", style: TextStyle(
                    color: Colors.white,
                    fontSize: 28.0,
                    fontWeight: FontWeight.w700
                ),),
                SizedBox(height: 10.0),
                Text("GRAFICI", style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),
                SizedBox(height: 10.0),
                Text("città", style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),




              ],
            ),
          )

        ],

      ),

    );
  }


  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(),
      child: _buildHeader(),
    );
  }
}