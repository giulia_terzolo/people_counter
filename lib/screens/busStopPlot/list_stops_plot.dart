import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../history.dart';




final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final CollectionReference _mainCollection = _firestore.collection('fermate');
FirebaseFirestore _db = FirebaseFirestore.instance;

class ListStopsPlot extends StatefulWidget {
  final email;
  final citta;
  ListStopsPlot({Key?key,this.email, this.citta}) : super(key: key);
  @override
  ListStopsPlotState createState() => ListStopsPlotState();
}


class ListStopsPlotState extends State<ListStopsPlot>{

  static Stream<QuerySnapshot> readItems(String email) {
    CollectionReference notesItemCollection = _mainCollection;
    return notesItemCollection.snapshots();
  }



  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: readItems(widget.email),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text("ERRORE");
        } else if (snapshot.hasData || snapshot.data != null) {
          return ListView.separated(
            separatorBuilder: (context, index) => SizedBox(height: 16.0),
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              //var noteInfo = snapshot.data!.docs[index].data()!;
              var noteInfo = snapshot.data!.docs[index];
              String docID = snapshot.data!.docs[index].id;

              String n = noteInfo['nome'];
              String cit = noteInfo['citta'];
              String description = noteInfo['sigla'];
              print(n);
              print(widget.email);
              print(docID);
              if(widget.citta==cit){
                return InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => History(email: widget.email, numFermata: description, citta: widget.citta,)),);

                  },
                  child: Container(
                    height: 80,
                    child: Card(
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Column(
                          children: <Widget>[
                            Container(
                              height: 30,
                              padding: const EdgeInsets.all(1),
                              child: ListTile(
                                title: Text(n, style: const TextStyle(fontSize:18.0,fontWeight: FontWeight.bold,),),
                              ),
                            ),
                          ]
                      ),
                    ),
                  ),
                );
              }else{
                return const SizedBox(height: 0,);
              }


            },
          );
        }

        return const Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Colors.deepOrangeAccent,
            ),
          ),
        );

      },
    );
  }
}