
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'appbar_stops_plot.dart';
import 'list_stops_plot.dart';


class HomeStopsPlot extends StatefulWidget {
  final email;
  final citta;
  HomeStopsPlot({Key?key,this.email, this.citta}) : super(key: key);
  @override
  _HomeStopsPlotState createState() =>  _HomeStopsPlotState();
}



class _HomeStopsPlotState extends State<HomeStopsPlot> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);




  @override
  Widget build(BuildContext context){
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));


    return Scaffold(

      appBar: AppBar(
        backgroundColor: color3,
        titleSpacing: 0.0,
        elevation: 0,
        title: AppBarStopsPlot(email: widget.email,citta: widget.citta,),
        toolbarHeight: 60.0,
      ),



      body: Theme(
        data: Theme.of(context).copyWith(
        ),

        //this creates blocks
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              bottom: 20.0,
              top: 20,
            ),
            child: ListStopsPlot(email: widget.email,citta: widget.citta,),
          ),

        ),


      ),



    );

  }





}