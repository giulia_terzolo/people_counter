import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../history.dart';

class CloudFirestoreSearchStopPlot extends StatefulWidget {
  final email;
  final citta;
  CloudFirestoreSearchStopPlot({Key?key,this.email, this.citta}) : super(key: key);
  @override
  _CloudFirestoreSearchStopPlotState createState() => _CloudFirestoreSearchStopPlotState();
}


class _CloudFirestoreSearchStopPlotState extends State<CloudFirestoreSearchStopPlot> {
  String name = "";



  @override
  Widget build(BuildContext context) {

    final Color color3 = const Color(0xffFB8964);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: color3,
        title: Card(
          child: TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(4),
            ],
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: color3),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: color3),
                ),

                prefixIcon: Icon(Icons.search, color: Colors.white,), hintText: 'Ricerca il codice della fermata...'),
            onChanged: (val) {
              setState(() {
                name = val;
              });
            },
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 16.0,
            right: 16.0,
            bottom: 20.0,
            top: 20.0,
          ),
          child: StreamBuilder<QuerySnapshot>(
            stream: (name != "" && name != null)
                ? FirebaseFirestore.instance
                .collection('fermate')
                .where("sigla", isEqualTo: name.toLowerCase())
                .snapshots()
                : FirebaseFirestore.instance.collection('fermate').snapshots(),
            builder: (context, snapshot) {
              return (snapshot.connectionState == ConnectionState.waiting)
                  ? const Center(child: CircularProgressIndicator())
                  :  ListView.separated(
                  separatorBuilder: (context, index) => SizedBox(height: 16.0),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    //var noteInfo = snapshot.data!.docs[index].data()!;
                    var noteInfo = snapshot.data!.docs[index];
                    String docID = snapshot.data!.docs[index].id;
                    String title = noteInfo['nome'];
                    String cit = noteInfo['citta'];
                    String description = noteInfo['sigla'];
                    if(widget.citta==cit){
                      return Ink(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: ListTile(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => History(email: widget.email, numFermata: description, citta: widget.citta,)),);

                          },
                          title: Text(
                            title,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle( fontWeight: FontWeight.bold),
                          ),
                          /*subtitle: Text(
                          description,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),*/
                        ),

                      );
                    }else{
                      return const SizedBox(height: 0,);
                    }

                  }

              );
            },
          ),
        ),
      ),
    );
  }

}