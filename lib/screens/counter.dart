import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'linea/home_linea.dart';



FirebaseFirestore _db = FirebaseFirestore.instance;

class Counter extends StatefulWidget {
  final email;
  final citta;
  final numFermata;
  const Counter({Key?key,this.email, this.citta, this.numFermata}) : super(key: key);
  @override
  _CounterState createState() => new _CounterState();
}


class _CounterState extends State<Counter> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);
  late int persone=0;



  @override
  Widget build(BuildContext context){
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));
    String cit=widget.citta;
    String ferm=widget.numFermata;

    return Scaffold(


      body: Theme(
        data: Theme.of(context).copyWith(
        ),

        //this creates blocks
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildHeader(),
              const SizedBox(height: 15.0),
              const Divider(height: 20, thickness: 2,),
              Row(
                children: <Widget>[
                  const SizedBox(width: 20.0),
                  const Text("CITTA' : ", style:  TextStyle(
                  color: Color(0xffFB8964),
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),
                  const Spacer(),
                  Text(cit.toUpperCase(), style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),),
                  const SizedBox(width: 20.0),
                ],
              ),
              const SizedBox(height: 10.0),
              Row(
                children: <Widget>[
                  const SizedBox(width: 20.0),
                  const Text("NUMERO FEMRATA : ", style:  TextStyle(
                    color: Color(0xffFB8964),
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),),
                  const Spacer(),
                  Text(ferm.toUpperCase(), style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),),
                  const SizedBox(width: 20.0),
                ],
              ),
              const Divider(height: 20, thickness: 2,),
              _buildButtons(),
              const Divider(height: 20, thickness: 2,),
              const SizedBox(height: 10.0),
              _buildButtonBus(),
              const SizedBox(height: 10.0),
              const Divider(height: 20, thickness: 2,),
              const SizedBox(height: 10.0),
              _buildButtonReset(),
              const SizedBox(height: 30.0),
            ],
          ),
        ),
      ),





    );
  }

  void aggiungiPersone({
    required String nomeCit,
    required String codFerm,
  })  async {
    DocumentSnapshot document1;
    try {
      String cod=nomeCit+"_"+codFerm;
      document1 = await   _db.collection('personePerFermata').doc(cod).get();
      print(document1.data());
      persone= document1.get("persone")+1;
      await _db.collection("personePerFermata").doc(cod).set({
        "persone": persone,
      });
      DateTime now = DateTime.now();
      aggiornoPersoneGrafico(nomeCit: nomeCit, codFerm: codFerm, temp: now);
      creaEvento(nomeCit: nomeCit, codFerm: codFerm,);
    } catch (e) {
      print(e);
    }
  }

  void togliPersone({
    required String nomeCit,
    required String codFerm,
  })  async {
    DocumentSnapshot document1;
    try {
      String cod=nomeCit+"_"+codFerm;
      document1 = await   _db.collection('personePerFermata').doc(cod).get();
      print(document1.data());
      persone= document1.get("persone");
      if(persone>0){
        persone = persone-1;
        await _db.collection("personePerFermata").doc(cod).set({
          "persone": persone,
        });
        DateTime now = DateTime.now();
        aggiornoPersoneGrafico(nomeCit: nomeCit, codFerm: codFerm, temp: now);
        creaEvento(nomeCit: nomeCit, codFerm: codFerm,);
      }
    } catch (e) {
      print(e);
    }
  }

  void creaEvento({
    required String nomeCit,
    required String codFerm,
  })  async {
    try {
      String cod=nomeCit+"_"+codFerm;
      DateTime now = DateTime.now();
      await _db.collection("eventi").doc(cod).collection("persone").doc(now.toString()).set({
        "numero": persone,
        "tempo": now,
      });
    } catch (e) {
      print(e);
    }
  }



  void aggiornoPersoneGrafico({
    required String nomeCit,
    required String codFerm,
    required DateTime temp,
  })  async {
    DocumentSnapshot document;
    try {
      String cod=nomeCit.toLowerCase()+"_"+codFerm.toLowerCase();
      document = await   _db.collection('grafici').doc(cod).get();
      print(document.data());

      int max= document.get("max");
      //10
      int p1= document.get("p1");
      Timestamp t1 = document.get("t1");
      int p2= document.get("p2");
      Timestamp t2 = document.get("t2");
      int p3= document.get("p3");
      Timestamp t3 = document.get("t3");
      int p4= document.get("p4");
      Timestamp t4 = document.get("t4");
      int p5= document.get("p5");
      Timestamp t5 = document.get("t5");
      int p6= document.get("p6");
      Timestamp t6 = document.get("t6");
      int p7= document.get("p7");
      Timestamp t7 = document.get("t7");
      int p8= document.get("p8");
      Timestamp t8 = document.get("t8");
      int p9= document.get("p9");
      Timestamp t9 = document.get("t9");

      //20
      int p11= document.get("p11");
      Timestamp t11 = document.get("t11");
      int p12= document.get("p12");
      Timestamp t12 = document.get("t12");
      int p13= document.get("p13");
      Timestamp t13 = document.get("t13");
      int p14= document.get("p14");
      Timestamp t14 = document.get("t14");
      int p15= document.get("p15");
      Timestamp t15 = document.get("t15");
      int p16= document.get("p16");
      Timestamp t16 = document.get("t16");
      int p17= document.get("p17");
      Timestamp t17 = document.get("t17");
      int p18= document.get("p18");
      Timestamp t18 = document.get("t18");
      int p19= document.get("p19");
      Timestamp t19 = document.get("t19");
      int p10= document.get("p10");
      Timestamp t10 = document.get("t10");

      //10
      Timestamp bt1 = document.get("bt1");
      Timestamp bt2 = document.get("bt2");
      Timestamp bt3 = document.get("bt3");
      Timestamp bt4 = document.get("bt4");
      Timestamp bt5 = document.get("bt5");

      //10
      String b1 = document.get("b1");
      String b2 = document.get("b2");
      String b3 = document.get("b3");
      String b4 = document.get("b4");
      String b5 = document.get("b5");

      int maxUp;
      if(persone>max){
        maxUp=persone;
      }
      else{
        maxUp=max;
      }



      await _db.collection("grafici").doc(cod).set({
        "max": maxUp,
        //10
        "p1": persone,
        "t1": temp,
        "p2": p1,
        "t2": t1,
        "p3": p2,
        "t3": t2,
        "p4": p3,
        "t4": t3,
        "p5": p4,
        "t5": t4,
        "p6": p5,
        "t6": t5,
        "p7": p6,
        "t7": t6,
        "p8": p7,
        "t8": t7,
        "p9": p8,
        "t9": t8,
        "p10":p9,
        "t10":t9,

        //20
        "p11": p10,
        "t11": t10,
        "p12": p11,
        "t12": t11,
        "p13": p12,
        "t13": t12,
        "p14": p13,
        "t14": t13,
        "p15": p14,
        "t15": t14,
        "p16": p15,
        "t16": t15,
        "p17": p16,
        "t17": t16,
        "p18": p17,
        "t18": t17,
        "p19": p18,
        "t19": t18,
        "p20":p19,
        "t20":t19,


        //10
        "bt1": bt1,
        "bt2": bt2,
        "bt3": bt3,
        "bt4": bt4,
        "bt5": bt5,

        //10
        "b1": b1,
        "b2": b2,
        "b3": b3,
        "b4": b4,
        "b5": b5,

      });
    } catch (e) {
      print(e);
    }
  }



  void reset({
    required String nomeCit,
    required String codFerm,
  })  async {
    try {
      String cod=nomeCit+"_"+codFerm;
      await _db.collection("personePerFermata").doc(cod).set({
        "persone": 0,
      });
      persone=0;
      creaEvento(nomeCit: nomeCit, codFerm: codFerm);
    } catch (e) {
      print(e);
    }
  }



  Container _buildButtonReset() {

    return  Container(
      child: Row(
        children: <Widget>[
          const Spacer(),
        FloatingActionButton.extended(
                onPressed: () {
                  reset(nomeCit: widget.citta, codFerm: widget.numFermata);
                },
                backgroundColor: color3,
                icon: const Icon(FontAwesomeIcons.broom, color: Colors.white, size: 20.0,),
                label: const Text(" RESET CONTATORE ", style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                    fontWeight: FontWeight.w700
                ),),
              ),
          const Spacer(),
        ],
      ),
    );
  }

  Container _buildButtonBus() {

    return  Container(
      child: Row(
        children: <Widget>[
          const Spacer(),
          FloatingActionButton.extended(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => HomeLinea(email: widget.email,numFermata: widget.numFermata,citta: widget.citta,)),);
            },
            backgroundColor: color3,
            icon: const Icon(FontAwesomeIcons.bus, color: Colors.white, size: 20.0,),
            label: const Text(" BUS ", style: TextStyle(
                color: Colors.white,
                fontSize: 25.0,
                fontWeight: FontWeight.w700
            ),),
          ),
          const Spacer(),
        ],
      ),
    );
  }


  Container _buildButtons() {
    return  Container(
      child: Row(
        children: <Widget>[
          const SizedBox(width: 20.0,),


          Container(
            height: 100.0,
            width: 100.0,
            child: FittedBox(
              child:     FloatingActionButton(
                  onPressed: () {
                    togliPersone(nomeCit: widget.citta, codFerm: widget.numFermata);
                  },
                  child: const Icon(FontAwesomeIcons.minus, color: Colors.white,),backgroundColor: const Color(0xffFB8964)),

            ),
          ),

          const Spacer(),

          Container(
            height: 100.0,
            width: 100.0,
            child: FittedBox(
              child:    FloatingActionButton(
                  onPressed: () {
                    aggiungiPersone(nomeCit: widget.citta, codFerm: widget.numFermata);
                  },
                  child: const Icon(FontAwesomeIcons.plus, color: Colors.white,),backgroundColor: const Color(0xffFB8964)),

            ),
          ),
       const SizedBox(width: 20.0,),
        ],
      ),
    );
  }




  Container _buildHeader() {
    String docName=widget.citta+"_"+widget.numFermata;
    // ignore: sized_box_for_whitespace
    return Container(
      height: 250,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Column(
            children: [
              const SizedBox(height: 60.0,),
              Row(
                children: [

                  const Spacer(),

                  FloatingActionButton(
                    backgroundColor: color3,
                    child: const Icon(Icons.arrow_back, size: 45.0, color: Colors.white,),
                    //Icon(FontAwesomeIcons.sistrix, size: 35.0,),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                  const SizedBox(width: 20.0,),



                ],
              ),
            ],
          ),

          Positioned(
            bottom: 0,
            left: -100,
            top: -150,
            child: Container(
              width: 350,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color1, color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color2,
                        offset: const Offset(4.0,4.0),
                        blurRadius: 10.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    colors: [color3,color2]
                ),
                boxShadow: [
                  BoxShadow(
                      color: color3,
                      offset: const Offset(1.0,1.0),
                      blurRadius: 4.0
                  )
                ]
            ),
          ),
          Positioned(
            top: 100,
            right: 200,
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color3,color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color3,
                        offset: const Offset(1.0,1.0),
                        blurRadius: 4.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 60,
                left: 30
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:  <Widget>[
                const Text("People Counter", style: TextStyle(
                    color: Colors.white,
                    fontSize: 28.0,
                    fontWeight: FontWeight.w700
                ),),
                const SizedBox(height: 10.0),
                const Text("CONTATORE", style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),
                const SizedBox(height: 10.0),
                StreamBuilder<DocumentSnapshot>(
                stream: FirebaseFirestore.instance
                    .collection('personePerFermata')
                    .doc(docName)
                    .snapshots(),
                builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (snapshot.hasData) {
                  print(snapshot);
                  String nome = snapshot.data!.get("persone").toString();
                  String testo="";
                  if(persone!=-1){
                    testo=persone.toString();
                  }else{
                    testo=nome;
                  }

                  return Text(nome, textAlign: TextAlign.center, style: const TextStyle(fontSize: 50.0, color: Colors.black, fontWeight: FontWeight.bold),);}
                else {
                return const Text("ERRORE");
                }}),

              ],
            ),
          )

        ],

      ),

    );
  }



}