import 'package:flutter/material.dart';
import 'search_stop.dart';


class AppBarStops extends StatefulWidget {
  final email;
  final citta;
  AppBarStops({Key?key,this.email, this.citta}) : super(key: key);
  @override
  _AppBarStopsState createState() =>  _AppBarStopsState();
}



class _AppBarStopsState extends State<AppBarStops> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);






  Container _buildHeader() {
    String testo="FERMATE PER "+widget.citta;
    // ignore: sized_box_for_whitespace
    return Container(
      height: 60,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Column(
            children: [
              //const SizedBox(height: 10.0,),
              Row(
                children: [
                  const SizedBox(width: 20.0,),
                   Text(testo.toUpperCase(), style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),),
                  const Spacer(),
                  IconButton(
                    //style: ElevatedButton.styleFrom(primary: color3, elevation: 0,),
                    icon: const Icon(Icons.search, size: 45.0, color: Colors.white,),
                    //Icon(FontAwesomeIcons.sistrix, size: 35.0,),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => CloudFirestoreSearchStop(email:  widget.email,citta: widget.citta,)),);
                    },
                  ),
                  const SizedBox(width: 20.0,),
                ],
              ),
            ],
          ),


              ],
      ),

    );
  }


  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(),
      child: _buildHeader(),

    );
  }
}