
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'add_fermata.dart';
import 'appbar_stops.dart';
import 'list_stops.dart';

class HomeStops extends StatefulWidget {
  final email;
  final citta;
  HomeStops({Key?key,this.email, this.citta}) : super(key: key);
  @override
  _HomeStopsState createState() =>  _HomeStopsState();
}



class _HomeStopsState extends State<HomeStops> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);




  @override
  Widget build(BuildContext context){
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));


    return Scaffold(

      appBar: AppBar(
        backgroundColor: color3,
        titleSpacing: 0.0,
        elevation: 0,
        title: AppBarStops(email: widget.email,citta: widget.citta,),
        toolbarHeight: 60.0,
      ),



      body: Theme(
        data: Theme.of(context).copyWith(
        ),

        //this creates blocks
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              bottom: 20.0,
              top: 20,
            ),
            child: ListStops(email: widget.email,citta: widget.citta,),
          ),

        ),


      ),

      //floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      floatingActionButton:
              FloatingActionButton(
                backgroundColor: color3,
                child: const Icon(FontAwesomeIcons.plus, size: 45.0, color: Colors.white,),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Addstopitem(email:  widget.email,citta: widget.citta,)),);
                },
              ),


    );

  }





}