import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../linea/add_linea_bus.dart';
import '../counter.dart';




final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final CollectionReference _mainCollection = _firestore.collection('fermate');
FirebaseFirestore _db = FirebaseFirestore.instance;

class ListStops extends StatefulWidget {
  final email;
  final citta;
  ListStops({Key?key,this.email, this.citta}) : super(key: key);
  @override
  ListStopsState createState() => ListStopsState();
}


class ListStopsState extends State<ListStops>{

  static Stream<QuerySnapshot> readItems(String email) {
    CollectionReference notesItemCollection = _mainCollection;
    return notesItemCollection.snapshots();
  }



  @override
  Widget build(BuildContext context) {
    final Color color3 = const Color(0xffFB8964);
    return StreamBuilder<QuerySnapshot>(
      stream: readItems(widget.email),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text("ERRORE");
        } else if (snapshot.hasData || snapshot.data != null) {
          return ListView.separated(
            separatorBuilder: (context, index) => SizedBox(height: 16.0),
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              //var noteInfo = snapshot.data!.docs[index].data()!;
              var noteInfo = snapshot.data!.docs[index];
              String docID = snapshot.data!.docs[index].id;

              String n = noteInfo['nome'];
              String s = noteInfo['sigla'];
              String cit = noteInfo['citta'];
              print(n);
              print(widget.email);
              print(docID);
              if(widget.citta==cit){
                return InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Counter(email:  widget.email,citta: widget.citta,numFermata: s,)),);

                  },
                  child: Container(
                    height: 80,
                    child: Card(
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Column(
                          children: <Widget>[
                            Container(
                              height: 37,
                              padding: const EdgeInsets.all(1),
                              child: ListTile(
                                title: Text(n, style: const TextStyle(fontSize:18.0,fontWeight: FontWeight.bold,),),
                                trailing: Container(
                                  height: 35.0,
                                  width: 35.0,
                                  child: FittedBox(
                                    child: FloatingActionButton(
                                      child:const Icon(FontAwesomeIcons.plus,size: 30.0, color: Colors.white,),
                                      backgroundColor: color3,
                                      onPressed: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => AddLinea(email: widget.email, numFermata: s, citta: widget.citta,)),);
                                      },),),),
                              ),
                            ),
                          ]
                      ),
                    ),
                  ),
                );
              }else{
                return const SizedBox(height: 0,);
              }


            },
          );
        }

        return const Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Colors.deepOrangeAccent,
            ),
          ),
        );

      },
    );
  }
}