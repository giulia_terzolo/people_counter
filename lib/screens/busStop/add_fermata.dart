
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


FirebaseFirestore _db = FirebaseFirestore.instance;

class Addstopitem extends StatefulWidget {
  final email;
  final citta;
  Addstopitem({Key?key,this.email, this.citta}) : super(key: key);
  @override
  AddstopitemState createState() => new AddstopitemState();
}



class AddstopitemState extends State<Addstopitem>{
  late String _nome;
  late String _sigla;
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  void setInfo({
    required String email,
    required String cit,
  })  async {
    try {

      await _db.collection("fermate").doc().set({
        "email": email,
        "citta": cit,
        "nome": _nome,
        "sigla":_sigla.toLowerCase(),
      });
    } catch (e) {
      print(e);
    }
  }

  void inizializzoPersonePerFermata({
    required String nomeCit,
    required String codFerm,
  })  async {
    try {
      String cod=nomeCit.toLowerCase()+"_"+codFerm.toLowerCase();
      await _db.collection("personePerFermata").doc(cod).set({
        "persone": 0,
      });
    } catch (e) {
      print(e);
    }
  }



  void inizializzoGrafico({
    required String nomeCit,
    required String codFerm,
  })  async {
    try {
      String cod=nomeCit.toLowerCase()+"_"+codFerm.toLowerCase();
      print("inizializzazione");
      print(cod);
      DateTime now = DateTime.now();
      int num=-1;
      String farloc="";
      await _db.collection('grafici').doc(cod).set({
        "max": 0,
        //10bus
        "bt1": now,
        "bt2": now,
        "bt3": now,
        "bt4": now,
        "bt5": now,

        //10busTitolo
        "b1": farloc,
        "b2": farloc,
        "b3": farloc,
        "b4": farloc,
        "b5": farloc,

        //10
        "p1": num,
        "t1": now,
        "p2": num,
        "t2": now,
        "p3": num,
        "t3": now,
        "p4": num,
        "t4": now,
        "p5": num,
        "t5": now,
        "p6": num,
        "t6": now,
        "p7": num,
        "t7": now,
        "p8": num,
        "t8": now,
        "p9": num,
        "t9": now,
        "p10":num,
        "t10":now,

        //20
        "p11": num,
        "t11": now,
        "p12": num,
        "t12": now,
        "p13": num,
        "t13": now,
        "p14": num,
        "t14": now,
        "p15": num,
        "t15": now,
        "p16": num,
        "t16": now,
        "p17": num,
        "t17": now,
        "p18": num,
        "t18": now,
        "p19": num,
        "t19": now,
        "p20":num,
        "t20":now,


      });
    } catch (e) {
      print(e);
    }
  }

  Widget _buildNomeCitta(){
    return TextFormField(
      keyboardType: TextInputType.text,
      //style:  TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),
      decoration:  InputDecoration(
        labelText: "Nome fermata",
        labelStyle:   TextStyle(color: color1, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        hintText: "Inserire nome per la fermata ...",
        hintStyle: TextStyle(
          color: color3,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "Il nome della fermata deve essere inserito";
        }
      }, onSaved: (String? value){
      _nome = value!;
    },
    );
  }


  Widget _buildSiglaCitta(){
    return TextFormField(
      keyboardType: TextInputType.number,
      inputFormatters: [
        LengthLimitingTextInputFormatter(4),
      ],
      //style:  TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),
      decoration:  InputDecoration(
        labelText: "Codice della fermata",
        labelStyle:   TextStyle(color: color1, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        hintText: "Inserire il codice della femrata ...",
        hintStyle: TextStyle(
          color: color3,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "Il codice della fermata deve essere inserito";
        }
      }, onSaved: (String? value){
      _sigla = value!;
    },
    );
  }




  @override
  Widget build(BuildContext context){
    print(widget.email);
    String testo= "AGGIUNGI FERMATA PER "+widget.citta;
    return Scaffold(
      appBar: AppBar(
        title:  Text(testo.toUpperCase() , style: const TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),),
        backgroundColor: color3,
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[

          Container(

            margin: const EdgeInsets.symmetric(),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 60,),
                  _buildNomeCitta(),
                  const SizedBox(height: 20,),
                  _buildSiglaCitta(),
                  const SizedBox(height: 50,),
                  RaisedButton(
                    color: Colors.red,
                    child: const Text("CREA",
                      style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
                    onPressed:(){
                      if(!_formKey.currentState!.validate()){
                        return;
                      }
                      _formKey.currentState!.save();
                      print(_nome);
                      print(_sigla);
                      setInfo(email: widget.email, cit: widget.citta);

                      inizializzoPersonePerFermata(nomeCit: widget.citta, codFerm: _sigla.toLowerCase());
                      inizializzoGrafico(nomeCit: widget.citta, codFerm: _sigla.toLowerCase());

                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.green, content: Text("Fermata inserita correttamente.",
                        style: TextStyle(color: Colors.black, ),)));
                        Navigator.pop(context);
                      //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Home(email: widget.email,),));
                    },

                  ),
                  const SizedBox(height: 20,),


                ],
              ),
            ),

          ),

        ],
      ),
    );
  }
}