import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:people_counter/screens/auxPlot/appbar_plots.dart';

import 'auxPlot/list_plots.dart';
import 'set.dart';
import 'home.dart';

class Plots extends StatefulWidget {
  final email;
  Plots({Key?key,this.email}) : super(key: key);
  @override
  _PlotsState createState() => new _PlotsState();
}


class _PlotsState extends State<Plots> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);



  @override
  Widget build(BuildContext context){

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));



    return Scaffold(

      appBar: AppBar(
        backgroundColor: Colors.transparent,
        titleSpacing: 0.0,
        elevation: 0,
        title: AppBarPlots(email: widget.email,),
        toolbarHeight: 225.0,
      ),



      body: Theme(
        data: Theme.of(context).copyWith(
        ),

        //this creates blocks
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              bottom: 20.0,
              top: 0,
            ),
            child: ListPlots(email: widget.email,),
          ),

        ),

      ),

      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: SizedBox(
          height: 50,
          child: Row(
            children: <Widget>[

              const SizedBox(width: 20.0),
              IconButton(
                //backgroundColor: color2,
                icon: const Icon(FontAwesomeIcons.cog),
                onPressed:  (){
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) =>  Set(email: widget.email,),
                      ));
                },
              ),
              const Spacer(),
              IconButton(
                //backgroundColor: color2,
                icon: const Icon(Icons.location_city_rounded, size: 30.0,  ),
                onPressed:  (){
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) =>  Home(email: widget.email,),
                      ));
                },
              ),
              const Spacer(),
              const SizedBox(width: 45.0),
              const SizedBox(width: 20.0),
            ],
          ),
        ),
      ),


      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: color2,
        child: const Icon(FontAwesomeIcons.chartLine),
        onPressed: (){},
      ),

    );
  }



  //HEADER for changes
  Container _buildHeader() {
    // ignore: sized_box_for_whitespace
    return Container(
      height: 250,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0,
            left: -100,
            top: -150,
            child: Container(
              width: 350,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color1, color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color2,
                        offset: const Offset(4.0,4.0),
                        blurRadius: 10.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    colors: [color3,color2]
                ),
                boxShadow: [
                  BoxShadow(
                      color: color3,
                      offset: const Offset(1.0,1.0),
                      blurRadius: 4.0
                  )
                ]
            ),
          ),
          Positioned(
            top: 100,
            right: 200,
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color3,color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color3,
                        offset: const Offset(1.0,1.0),
                        blurRadius: 4.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 60,
                left: 30
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const <Widget>[
                Text("People Counter", style: TextStyle(
                    color: Colors.white,
                    fontSize: 28.0,
                    fontWeight: FontWeight.w700
                ),),
                SizedBox(height: 10.0),
                Text("PLOTS", style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),),

              ],
            ),
          )
        ],
      ),
    );
  }
}