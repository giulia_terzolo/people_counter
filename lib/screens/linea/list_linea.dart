import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';




final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final CollectionReference _mainCollection = _firestore.collection('lineaFermata');
FirebaseFirestore _db = FirebaseFirestore.instance;

class ListLinea extends StatefulWidget {
  final email;
  final citta;
  final numFermata;
  ListLinea({Key?key,this.email, this.citta, this.numFermata}) : super(key: key);
  @override
  ListLineaState createState() => ListLineaState();
}

class ListLineaState extends State<ListLinea>{



  static Stream<QuerySnapshot> readItems(String numFermata, String citta) {
    String docID=citta+"_"+numFermata;
    CollectionReference notesItemCollection = _mainCollection.doc(docID).collection("linea");
    return notesItemCollection.snapshots();
  }

  void creaEvento({
    required String nomeCit,
    required String codFerm,
    required String descrizione,
  })  async {
    try {
      String cod=nomeCit+"_"+codFerm;
      DateTime now = DateTime.now();

      await _db.collection("eventi").doc(cod).collection("bus").doc(now.toString()).set({
        "linea": descrizione,
        "tempo": now,
      });
    } catch (e) {
      print(e);
    }
  }

  void aggiornoBusGrafico({
    required String nomeCit,
    required String codFerm,
    required DateTime temp,
    required String linea,
  })  async {
    DocumentSnapshot document;
    try {
      String cod=nomeCit.toLowerCase()+"_"+codFerm.toLowerCase();
      document = await   _db.collection('grafici').doc(cod).get();
      print(document.data());
      int max= document.get("max");
      //10
      int p1= document.get("p1");
      print("setting");
      print(cod);
      print(p1);
      Timestamp t1 = document.get("t1");
      int p2= document.get("p2");
      Timestamp t2 = document.get("t2");
      int p3= document.get("p3");
      Timestamp t3 = document.get("t3");
      int p4= document.get("p4");
      Timestamp t4 = document.get("t4");
      int p5= document.get("p5");
      Timestamp t5 = document.get("t5");
      int p6= document.get("p6");
      Timestamp t6 = document.get("t6");
      int p7= document.get("p7");
      Timestamp t7 = document.get("t7");
      int p8= document.get("p8");
      Timestamp t8 = document.get("t8");
      int p9= document.get("p9");
      Timestamp t9 = document.get("t9");
      int p10= document.get("p10");
      Timestamp t10 = document.get("t10");

      //20
      int p11= document.get("p11");
      Timestamp t11 = document.get("t11");
      int p12= document.get("p12");
      Timestamp t12 = document.get("t12");
      int p13= document.get("p13");
      Timestamp t13 = document.get("t13");
      int p14= document.get("p14");
      Timestamp t14 = document.get("t14");
      int p15= document.get("p15");
      Timestamp t15 = document.get("t15");
      int p16= document.get("p16");
      Timestamp t16 = document.get("t16");
      int p17= document.get("p17");
      Timestamp t17 = document.get("t17");
      int p18= document.get("p18");
      Timestamp t18 = document.get("t18");
      int p19= document.get("p19");
      Timestamp t19 = document.get("t19");
      int p20= document.get("p20");
      Timestamp t20 = document.get("t20");

      //10
      Timestamp bt1 = document.get("bt1");
      Timestamp bt2 = document.get("bt2");
      Timestamp bt3 = document.get("bt3");
      Timestamp bt4 = document.get("bt4");

      //10
      String b1 = document.get("b1");
      String b2 = document.get("b2");
      String b3 = document.get("b3");
      String b4 = document.get("b4");


      await _db.collection("grafici").doc(cod).set({
        "max": max,
        //10
        "p1": p1,
        "t1": t1,
        "p2": p2,
        "t2": t2,
        "p3": p3,
        "t3": t3,
        "p4": p4,
        "t4": t4,
        "p5": p5,
        "t5": t5,
        "p6": p6,
        "t6": t6,
        "p7": p7,
        "t7": t7,
        "p8": p8,
        "t8": t8,
        "p9": p9,
        "t9": t9,
        "p10":p10,
        "t10":t10,

        //20
        "p11": p11,
        "t11": t11,
        "p12": p12,
        "t12": t12,
        "p13": p13,
        "t13": t13,
        "p14": p14,
        "t14": t14,
        "p15": p15,
        "t15": t15,
        "p16": p16,
        "t16": t16,
        "p17": p17,
        "t17": t17,
        "p18": p18,
        "t18": t18,
        "p19": p19,
        "t19": t19,
        "p20":p20,
        "t20":t20,

        //10
        "bt1": t1,
        "bt2": bt1,
        "bt3": bt2,
        "bt4": bt3,
        "bt5": bt4,

        //10
        "b1": linea,
        "b2": b1,
        "b3": b2,
        "b4": b3,
        "b5": b4,

      });
    } catch (e) {
      print(e);
    }
  }



  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: readItems(widget.numFermata, widget.citta),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text("ERRORE");
        } else if (snapshot.hasData || snapshot.data != null) {
          return ListView.separated(
            separatorBuilder: (context, index) => SizedBox(height: 16.0),
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              //var noteInfo = snapshot.data!.docs[index].data()!;
              var noteInfo = snapshot.data!.docs[index];
              String docID = snapshot.data!.docs[index].id;

              String n = noteInfo['linea'];
              String linea = "linea "+n;
              print(n);
              print(widget.email);
              print(docID);
                return InkWell(
                  onTap: (){
                    DateTime now = DateTime.now();
                    aggiornoBusGrafico(nomeCit: widget.citta, codFerm: widget.numFermata, temp: now, linea: linea);
                    creaEvento(nomeCit: widget.citta, codFerm: widget.numFermata, descrizione: linea);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.green, content: Text("Evento bus inserito.",
                      style: TextStyle(color: Colors.black, ),)));
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 80,
                    child: Card(
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Column(
                          children: <Widget>[
                            Container(
                              height: 30,
                              padding: const EdgeInsets.all(1),
                              child: ListTile(
                                title: Text(n, style: const TextStyle(fontSize:18.0,fontWeight: FontWeight.bold,),),
                                trailing: const Icon(FontAwesomeIcons.bus),
                              ),
                            ),
                          ]
                      ),
                    ),
                  ),
                );



            },
          );
        }

        return const Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Colors.deepOrangeAccent,
            ),
          ),
        );

      },
    );
  }
}