import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CloudFirestoreSearchLinea extends StatefulWidget {
  final email;
  final citta;
  final numFermata;
  CloudFirestoreSearchLinea({Key?key,this.email, this.citta, this.numFermata}) : super(key: key);
  @override
  _CloudFirestoreSearchLineaState createState() => _CloudFirestoreSearchLineaState();
}


class _CloudFirestoreSearchLineaState extends State<CloudFirestoreSearchLinea> {
  String name = "";

  void creaEvento({
    required String nomeCit,
    required String codFerm,
    required String nomeEvento,
    required String descrizione,
  })  async {
    try {
      String cod=nomeCit+"_"+codFerm;
      DateTime now = DateTime.now();
      FirebaseFirestore _db = FirebaseFirestore.instance;
      await _db.collection("eventi").doc(cod).collection("eventiPerCittaFermata").doc().set({
        "nome": nomeEvento,
        "descrizione": descrizione,
        "tempo": now,
      });
    } catch (e) {
      print(e);
    }
  }



  @override
  Widget build(BuildContext context) {
    String docID= widget.citta+"_"+widget.numFermata;

    final Color color3 = const Color(0xffFB8964);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: color3,
        title: Card(
          child: TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(4),
            ],
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: color3),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: color3),
                ),

                prefixIcon: const Icon(Icons.search, color: Colors.white,), hintText: 'Ricerca il codice della linea...'),
            onChanged: (val) {
              setState(() {
                name = val;
              });
            },
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 16.0,
            right: 16.0,
            bottom: 20.0,
            top: 20.0,
          ),
          child: StreamBuilder<QuerySnapshot>(
            stream: (name != "" && name != null)
                ? FirebaseFirestore.instance
                .collection('lineaFermata').doc(docID).collection("linea")
                .where("linea", isEqualTo: name.toLowerCase())
                .snapshots()
                : FirebaseFirestore.instance.collection('lineaFermata').doc(docID).collection("linea").snapshots(),
            builder: (context, snapshot) {
              return (snapshot.connectionState == ConnectionState.waiting)
                  ? const Center(child: CircularProgressIndicator())
                  :  ListView.separated(
                  separatorBuilder: (context, index) => SizedBox(height: 16.0),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    //var noteInfo = snapshot.data!.docs[index].data()!;
                    var noteInfo = snapshot.data!.docs[index];
                    String docID = snapshot.data!.docs[index].id;
                    String title = noteInfo['linea'];
                      return Ink(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: ListTile(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          onTap: (){
                            creaEvento(nomeCit: widget.citta, codFerm: widget.numFermata, nomeEvento: "bus", descrizione: title);
                            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.green, content: Text("Evento bus inserito.",
                              style: TextStyle(color: Colors.black, ),)));
                            Navigator.pop(context);
                          },
                          title: Text(
                            title,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle( fontWeight: FontWeight.bold),
                          ),
                          trailing: const Icon(FontAwesomeIcons.bus),
                          /*subtitle: Text(
                          description,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),*/
                        ),

                      );


                  }

              );
            },
          ),
        ),
      ),
    );
  }

}