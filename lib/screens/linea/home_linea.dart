
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'appbar_linea.dart';
import 'list_linea.dart';



class HomeLinea extends StatefulWidget {
  final email;
  final citta;
  final numFermata;
  HomeLinea({Key?key,this.email, this.citta, this.numFermata}) : super(key: key);
  @override
  _HomeLineaState createState() =>  _HomeLineaState();
}



class _HomeLineaState extends State<HomeLinea> {
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);




  @override
  Widget build(BuildContext context){
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xffFA8165),
    ));


    return Scaffold(

      appBar: AppBar(
        backgroundColor: color3,
        titleSpacing: 0.0,
        elevation: 0,
        title: AppBarLinea(email: widget.email,citta: widget.citta,numFermata: widget.numFermata,),
        toolbarHeight: 60.0,
      ),



      body: Theme(
        data: Theme.of(context).copyWith(
        ),

        //this creates blocks
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              bottom: 20.0,
              top: 20,
            ),
            child: ListLinea(email: widget.email,citta: widget.citta,numFermata: widget.numFermata,),
          ),

        ),


      ),



    );

  }





}