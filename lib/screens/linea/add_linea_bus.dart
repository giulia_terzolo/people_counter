
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


FirebaseFirestore _db = FirebaseFirestore.instance;

class AddLinea extends StatefulWidget {
  final email;
  final citta;
  final numFermata;

  AddLinea({Key?key,this.email, this.citta, this.numFermata}) : super(key: key);
  @override
  AddLineaState createState() => new AddLineaState();
}



class AddLineaState extends State<AddLinea>{
  late String _linea;
  final Color color1 = const Color(0xffFA696C);
  final Color color2 = const Color(0xffFA8165);
  final Color color3 = const Color(0xffFB8964);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  void setInfo({
    required String email,
    required String citta,
    required String numFermata,
  })  async {
    try {
      String docId = citta+"_"+numFermata;
      await _db.collection("lineaFermata").doc(docId).collection("linea").doc().set({
        "email": email,
        "linea": _linea.toLowerCase(),
      });
    } catch (e) {
      print(e);
    }
  }




  Widget _buildLinea(){
    return TextFormField(
      keyboardType: TextInputType.text,
      inputFormatters: [
        LengthLimitingTextInputFormatter(4),
      ],
      //style:  TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),
      decoration:  InputDecoration(
        labelText: "Linea del bus",
        labelStyle:   TextStyle(color: color1, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color3),
        ),
        hintText: "Inserire la linea del bus passante per la fermata ...",
        hintStyle: TextStyle(
          color: color3,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "La linea del bus deve essere inserita";
        }
      }, onSaved: (String? value){
      _linea = value!;
    },
    );
  }




  @override
  Widget build(BuildContext context){
    print(widget.email);
    String testo="AGGIUNGI LINEA PER "+widget.numFermata;
    return Scaffold(
      appBar: AppBar(
        title: Text(testo, style: const TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),),
        backgroundColor: color3,
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[

          Container(

            margin: const EdgeInsets.symmetric(),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 60,),
                  _buildLinea(),
                  const SizedBox(height: 50,),
                  RaisedButton(
                    color: Colors.red,
                    child: const Text("CREA",
                      style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
                    onPressed:(){
                      if(!_formKey.currentState!.validate()){
                        return;
                      }
                      _formKey.currentState!.save();
                      print(_linea);
                      setInfo(email: widget.email, citta: widget.citta, numFermata: widget.numFermata);
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.green, content: Text("Linea di bus per la fermata inserita correttamente.",
                        style: TextStyle(color: Colors.black, ),)));
                      Navigator.pop(context);
                      //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Home(email: widget.email,),));
                    },

                  ),
                  const SizedBox(height: 20,),


                ],
              ),
            ),

          ),

        ],
      ),
    );
  }
}