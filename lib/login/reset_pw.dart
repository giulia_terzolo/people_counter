import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'login.dart';


FirebaseFirestore _db = FirebaseFirestore.instance;

class ResetPage extends StatefulWidget{
  const ResetPage({Key? key}) : super(key: key);

  @override
  _ResetPageState createState() => _ResetPageState();
}

class _ResetPageState extends State<ResetPage>{
  late String _email;
  final auth = FirebaseAuth.instance;




  @override
  Widget build(BuildContext context) {
    return Scaffold(


      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffFA696C),
              Color(0xffFA8165),
              Color(0xffFB8964),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),

        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              //bottom: 20.0,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 9,
                        child: Image.asset(
                          'assets/icon/icon.png',
                          height: 150,
                        ),
                      ),
                      const Flexible(
                        flex: 1,
                        child: SizedBox(height: 20),
                      ),


                      Flexible(
                        flex: 0,
                        child: Text(
                          'autoBUS',
                          style: TextStyle(
                            //color: Color(0xFFD32F2F),
                            color: Colors.pinkAccent.shade400,
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      TextField(
                          keyboardType: TextInputType.emailAddress,
                          style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
                          decoration: const InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            hintText: "Inserire email ...",
                            hintStyle: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onChanged: (value) {
                            setState(() {
                              _email = value.trim();
                            }
                            );
                          }

                      ),


                      const SizedBox(
                        height: 30,
                        width: 250,
                      ),


                      Container(
                        height: 50,
                        width: 280,
                        decoration: BoxDecoration(
                            color: Colors.redAccent, borderRadius: BorderRadius.circular(20)),
                        child: FlatButton(
                          onPressed: () {
                            auth.sendPasswordResetEmail(email: _email);
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => const LoginPage(),
                                ));
                          },
                          child: const Text('Manda richiesta',
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        ),
                      ),

                      const SizedBox(
                        height: 10,
                        width: 250,
                      ),

                      Container(
                        height: 50,
                        width: 280,
                        decoration: BoxDecoration(
                            color: Colors.red, borderRadius: BorderRadius.circular(20)),
                        child: FlatButton(
                          onPressed: (){
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => const LoginPage(),
                                ));
                          },
                          child: Text("INDIETRO", style: TextStyle(color: Colors.white, fontSize: 20),),
                        ),
                      ),

                    ],

                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }

}