

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:people_counter/screens/home.dart';

FirebaseFirestore _db = FirebaseFirestore.instance;

class UserInfoForm extends StatefulWidget{
  final email;
  UserInfoForm({Key?key,this.email}) : super(key: key);
  @override
  UserInfoFormState createState() => UserInfoFormState();
}


class UserInfoFormState extends State<UserInfoForm>{
  late String _nome;
  late String _cogn;
  late String _nikname;
  late String _tel;


  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  void setInfo({
    required String email,
  })  async {
    DocumentSnapshot document;
    try {
      document = await   _db.collection('utenti').doc(email).get();
      print(document.data());
      String _e = document.get("email");
      _db.collection('utenti').doc(email).set(<String, Object>{
        'nome': _nome,
        'nick': _nikname,
        'cognome': _cogn,
        "tel": _tel,
        'email': _e,

      });
    } catch (e) {
      print(e);
    }
  }



  Widget _buildNome(){
    return TextFormField(
      keyboardType: TextInputType.name,
      style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
      decoration: const InputDecoration(
        labelText: "Nome",
        labelStyle:   TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        hintText: "Inserire nome ...",
        hintStyle: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "Il nome deve essere inserito";
        }
      }, onSaved: (String? value){
      _nome = value!;
    },
    );
  }
  Widget _buildCogn(){
    return TextFormField(
      keyboardType: TextInputType.name,
      style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
      decoration: const InputDecoration(
        labelText: "Cognome",
        labelStyle:   TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        hintText: "Inserire cognome ...",
        hintStyle: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "Il cognome deve essere inserito";
        }
      }, onSaved: (String? value){
      _cogn = value!;
    },
    );
  }

  Widget _buildNick(){
    return TextFormField(
      keyboardType: TextInputType.text,
      style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
      decoration: const InputDecoration(
        labelText: "Nome utente",
        labelStyle:   TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        hintText: "Inserire nome utente ...",
        hintStyle: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
        ),
      ),
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "Il nome utente deve essere inserito";
        }
      }, onSaved: (String? value){
      _nikname = value!;
    },
    );
  }

  Widget _buildTel(){
    return TextFormField(
      style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
      decoration: const InputDecoration(
        labelText: "Telefono",
        labelStyle:   TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        hintText: "Inserire il numero di telefono ...",
        hintStyle: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
        ),
      ),
      inputFormatters: <TextInputFormatter>[
        WhitelistingTextInputFormatter.digitsOnly,
      ],
      keyboardType: TextInputType.phone,
      validator: (String? value){
        if(value == null || value.isEmpty){
          return "Il numero di telefono deve essere inserito";
        }
      }, onSaved: (String? value){
      _tel = value! ;
    },
    );
  }


  @override
  Widget build(BuildContext context){
    print(widget.email);
    return Scaffold(
      body: Container( decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xffFA696C),
            Color(0xffFA8165),
            Color(0xffFB8964),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),

        child: ListView(
            padding: const EdgeInsets.all(8),
            children: <Widget>[

        Container(

        margin: const EdgeInsets.symmetric(),
        child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(height: 60,),
                    _buildNome(),
                    const SizedBox(height: 20,),
                    _buildCogn(),
                    const SizedBox(height: 20,),
                    _buildNick(),
                    const SizedBox(height: 20,),
                    _buildTel(),
                    const SizedBox(height: 50,),
                    RaisedButton(
                      color: Colors.red,
                      child: const Text("INVIA",
                        style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
                      onPressed:(){
                        if(!_formKey.currentState!.validate()){
                          return;
                        }
                        _formKey.currentState!.save();
                        print(_nome);
                        print(_cogn);
                        print(_nikname);
                        print(_tel);
                        setInfo(email: widget.email);
                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Home(email: widget.email,),));
                      },

                    ),

                  ],
                ),
              ),

    ),

          ],
      ),
    ),
    );
  }
}