


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'form_info_user.dart';
import '../screens/home.dart';
import 'new_user_screen.dart';
import 'reset_pw.dart';

class LoginPage extends StatefulWidget{
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{
  late String _email;
  late String _password;

  _showAlertDialog(errorMsg) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.deepOrangeAccent,
            title: const Text('Login Failed', style:  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            content: Text(errorMsg.toString(), style: const TextStyle(color: Colors.white, ),),
          );
        });
  }

  static Future<void> addItem({
    required String email,
  }) async {
    try {
      FirebaseFirestore _db = FirebaseFirestore.instance;
      await _db.collection('utenti').doc(email).set({
        'email': email,
        'nome': "",
        'nick': "",
        'cognome': "",
        "tel": "",
      });

    } catch (e) {
      print(e);
    }
  }

  void createHome({
    required String email,
  })  async {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => Home(email: email,),
        ));
  }

  void createNewUserForm({
    required String email,
  })  async {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => UserInfoForm(email: email,),
        ));
  }

  Future<UserCredential> _signInWithGoogle() async {

    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
    final GoogleSignInAuthentication googleAuth = await googleUser!.authentication;
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    FirebaseFirestore _db = FirebaseFirestore.instance;
    _email=googleUser.email;
    DocumentSnapshot document = await   _db.collection('utenti').doc(_email).get();

    if (document.exists){
      checkAuthenticationLogin(email: _email);
    } else{
      addItem( email: _email);
      createNewUserForm(email: _email);
    }
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }


  void checkAuthenticationLogin({
    required String email,
  })  async {
    DocumentSnapshot document;
    try {
      FirebaseFirestore _db = FirebaseFirestore.instance;
      document = await   _db.collection('utenti').doc(email).get();
      print(document.data());
      String _nick = document.get("nick");
      if (document.exists && _nick==""){
        createNewUserForm(email: _email);
      }else {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => Home(email: email,),));
      }

    } catch (e) {
      print(e);
    }
  }




  Future<void> _login() async{
    try{

      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(email: _email, password: _password);
      print("User $userCredential");
      checkAuthenticationLogin(email: _email);

    } on FirebaseAuthException catch(e){
      print("Error: $e");
      _showAlertDialog(e);

    } catch(e) {
      print("Error: $e");
      _showAlertDialog(e);
    }
  }




  Widget _buildForgotPasswordBtn() {
    return Center(
      //alignment: Alignment.centerRight,
      child:
      FlatButton(
        onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ResetPage()),),
        padding: const EdgeInsets.only(right: 0.0),
        child: const Text('Password dimenticata?', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),),
      ),
    );
  }

  Widget googleB()  {
    if(kIsWeb){
      return    const SizedBox(height: 0.0,);
    }else{
      return
        Container(
          height: 50,
          width: 280,
          decoration: BoxDecoration(
              color: Colors.redAccent, borderRadius: BorderRadius.circular(20)),
          child: FlatButton(
            onPressed: _signInWithGoogle,
            child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                            image:
                            AssetImage('assets/logos/google.png'),
                            fit: BoxFit.cover),
                        shape: BoxShape.circle,
                      ),
                    ),
                    const Text('Entra con Google', style: TextStyle(color: Colors.white, fontSize: 20),),
                  ],
                )
            ),
          ),
        );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(


      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffFA696C),
              Color(0xffFA8165),
              Color(0xffFB8964),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),

        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              //bottom: 20.0,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 9,
                        child: Image.asset(
                          'assets/icon/icon.png',
                          height: 150,
                        ),
                      ),
                      const Flexible(
                        flex: 1,
                        child: SizedBox(height: 20),
                      ),


                      Flexible(
                        flex: 0,
                        child: Text(
                          'autoBUS',
                          style: TextStyle(
                            //color: Color(0xFFD32F2F),
                            color: Colors.pinkAccent.shade400,
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextField(
                        onChanged: (value){
                          _email = value;
                        },
                        keyboardType: TextInputType.emailAddress,
                        style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
                        decoration: const InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.red),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.red),
                          ),
                          hintText: "Inserire email ...",
                          hintStyle: TextStyle(
                            color: Colors.red,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      TextField(
                        onChanged: (value){
                          _password = value;
                        },
                        style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
                        obscureText: true,
                        decoration: const InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            hintStyle: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                            ),
                            hintText: "Inserire password ..."
                        ),
                      ),

                      const SizedBox(
                        height: 30,
                        width: 250,
                      ),
                      Container(
                        height: 50,
                        width: 280,
                        decoration: BoxDecoration(
                            color: Colors.redAccent, borderRadius: BorderRadius.circular(20)),
                        child: FlatButton(
                          onPressed: _login,
                          child: const Text(
                            'Login',
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                        width: 250,
                      ),

                      Container(
                        height: 50,
                        width: 280,
                        decoration: BoxDecoration(
                            color: Colors.redAccent, borderRadius: BorderRadius.circular(20)),
                        child: FlatButton(
                          onPressed: (){
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => const CreatePage(),
                                ));
                          } ,
                          child: const Text("Crea nuovo account", style:  TextStyle(color: Colors.white, fontSize: 20),),
                        ),
                      ),

                      const SizedBox(
                        height: 10,
                        width: 250,
                      ),
                      googleB(),

                      _buildForgotPasswordBtn(),

                    ],

                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }

}