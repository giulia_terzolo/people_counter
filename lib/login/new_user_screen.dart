import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:people_counter/screens/home.dart';

import 'form_info_user.dart';
import 'login.dart';

FirebaseFirestore _db = FirebaseFirestore.instance;

class CreatePage extends StatefulWidget{
  const CreatePage({Key? key}) : super(key: key);

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage>{
  late String _email;
  late String _password;


  _showAlertDialog(errorMsg) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.deepOrangeAccent,
            title: const Text('Login Failed', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            content: Text(errorMsg.toString(), style: const TextStyle(color: Colors.white),),
          );
        });
  }



  static Future<void> addItemNew({
    required String email,
  }) async {
    try {
      await _db.collection('utenti').doc(email).set({
        'email': email,
        'nome': "",
        'nick': "",
        'cognome': "",
        "tel": "",
      });
    } catch (e) {
      print(e);
    }
  }


  Future<void> _createUser() async{
    try{
      print("Email: $_email Password: $_password");
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _password);
      print("User $userCredential");
      addItemNew(email: _email);
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => UserInfoForm(email: _email,),));
    } on FirebaseAuthException catch(e){
      print("Error: $e");
      _showAlertDialog(e);
    } catch(e) {
      print("Error: $e");
      _showAlertDialog(e);
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(


      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffFA696C),
              Color(0xffFA8165),
              Color(0xffFB8964),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),

        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              //bottom: 20.0,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 9,
                        child: Image.asset(
                          'assets/icon/icon.png',
                          height: 150,
                        ),
                      ),
                      const Flexible(
                        flex: 1,
                        child: SizedBox(height: 20),
                      ),


                      Flexible(
                        flex: 0,
                        child: Text(
                          'autoBUS',
                          style: TextStyle(
                            //color: Color(0xFFD32F2F),
                            color: Colors.pinkAccent.shade400,
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      TextField(
                        onChanged: (value){
                          _email = value;
                        },
                        keyboardType: TextInputType.emailAddress,
                        style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
                        decoration: const InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.red),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.red),
                          ),
                          hintText: "Inserire email ...",
                          hintStyle: TextStyle(
                            color: Colors.red,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      TextField(
                        onChanged: (value){
                          _password = value;
                        },
                        style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold,),
                        obscureText: true,
                        decoration: const InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            hintStyle: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                            ),
                            hintText: "Inserire password ..."
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                        width: 250,
                      ),


                      Container(
                        height: 50,
                        width: 280,
                        decoration: BoxDecoration(
                            color: Colors.redAccent, borderRadius: BorderRadius.circular(20)),
                        child: FlatButton(
                          onPressed: _createUser,
                          child: Text("Crea nuovo account", style: TextStyle(color: Colors.white, fontSize: 20),),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                        width: 250,
                      ),

                      Container(
                        height: 50,
                        width: 280,
                        decoration: BoxDecoration(
                            color: Colors.red, borderRadius: BorderRadius.circular(20)),
                        child: FlatButton(
                          onPressed: (){
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => const LoginPage(),
                                ));
                          },
                          child: Text("INDIETRO", style: TextStyle(color: Colors.white, fontSize: 20),),
                        ),
                      ),

                    ],

                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }

}